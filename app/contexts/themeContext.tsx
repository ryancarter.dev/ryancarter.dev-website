import React, { createContext, useState } from "react";
import { userPrefsCookie } from "~/cookies";
import { Theme } from "~/types/theme";

interface ThemeContextProps {
  name: Theme;
  setTheme: (theme: Theme) => void;
}

export const ThemeContext = createContext<ThemeContextProps>({
  name: "green",
  setTheme: () => {},
});

export const ThemeProvider = ({
  value,
  children,
}: {
  value: Theme;
  children: React.ReactNode;
}) => {
  const [theme, setTheme] = useState(value);

  const setThemeAndCommit = async (theme: Theme) => {
    setTheme(theme);
    document.cookie = await userPrefsCookie.serialize({ theme: theme });
  };

  return (
    <ThemeContext.Provider value={{ name: theme, setTheme: setThemeAndCommit }}>
      {children}
    </ThemeContext.Provider>
  );
};
