export default interface Message {
  id: string;
  message: JSX.Element;
  type: "question" | "answer";
}
