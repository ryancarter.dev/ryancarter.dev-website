import { createCookie } from "@remix-run/cloudflare";

export const userPrefsCookie = createCookie("user-prefs", {
  maxAge: 604_800, // one week
});
