import { ReactElement, ReactNode, useContext } from "react";
import { ThemeContext } from "~/contexts/themeContext";

export const themeClasses = {
  green: "theme-green",
  blue: "theme-blue",
};

function Theme({ children }: { children: ReactNode }): ReactElement {
  const { name } = useContext(ThemeContext);
  return (
    <body
      className={`bg-skin-base font-normal text-skin-base ${themeClasses[name]}`}
    >
      {children}
    </body>
  );
}

export default Theme;
