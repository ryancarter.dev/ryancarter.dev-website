import { ComponentStory, ComponentMeta } from "@storybook/react";
import Conversation, { ConversationProps } from "./conversation";

export default {
  title: "Components/Conversation",
  component: Conversation,
} as ComponentMeta<typeof Conversation>;

const Template: ComponentStory<typeof Conversation> = (
  args: ConversationProps
) => <Conversation {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  messages: [
    {
      id: "1",
      message: <p>This is a question?</p>,
      type: "question",
    },
    {
      id: "2",
      message: <p>This is an answer"</p>,
      type: "answer",
    },
  ],
};
