import Message from "~/types/message";
import EmailSender from "../emailSender/emailSender";
import TextMessage from "../textMessage/textMessage";

export interface ConversationProps {
  messages: Message[];
}

export default function Conversation({ messages }: ConversationProps) {
  return (
    <div className="grid gap-4 text-skin-inverted md:gap-6">
      {messages.map((message) => (
        <TextMessage key={message.id} {...message} />
      ))}
      <EmailSender className="mt-4" />
    </div>
  );
}
