import { ReactNode } from "react";

export interface ContainerProps {
  children: ReactNode;
  bgTransparent?: boolean;
  className?: string;
}

export default function Container({
  children,
  bgTransparent = false,
  className,
}: ContainerProps) {
  return (
    <div
      className={`mx-4 flex max-w-5xl flex-col items-center rounded-xl lg:mx-auto ${
        bgTransparent || "bg-skin-container p-3 shadow-md"
      } ${className}`}
    >
      {children}
    </div>
  );
}
