import { ComponentStory, ComponentMeta } from "@storybook/react";
import Button from "../button/button";

import Container, { ContainerProps } from "./container";

export default {
  title: "Components/Container",
  component: Container,
} as ComponentMeta<typeof Container>;

const Template: ComponentStory<typeof Container> = (args: ContainerProps) => (
  <Container {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  children: (
    <div>
      <p className="text-skin-base">Some container content</p>
      <p className="text-skin-base">Some container content</p>
      <p className="text-skin-base">Some container content</p>
      <p className="text-skin-base">Some container content</p>
      <Button>Button</Button>
    </div>
  ),
};
