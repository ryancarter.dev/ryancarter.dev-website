export default function SpeechBubbleCorner() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 38.93 35"
      style={{ width: "100%", height: "100%" }}
    >
      <g id="Layer_2" data-name="Layer 2" fill="currentColor">
        <g id="Layer_1-2" data-name="Layer 1">
          <path
            fill="currentColor"
            d="M32,0c0,6.15,0,12.3-.16,18.45s.52,12,7.09,16.05c-7,1.6-11.75-1.18-16.84-4.74C15.52,34.4,8,35.06,0,35"
          />
        </g>
      </g>
    </svg>
  );
}
