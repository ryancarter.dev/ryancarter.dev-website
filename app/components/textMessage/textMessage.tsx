import SpeechBubbleCorner from "~/components/svg/speechBubbleCorner";
import Message from "~/types/message";

export default function TextMessage({ message, type }: Message) {
  const isQuestion = type === "question";

  return (
    <div
      className={`relative w-fit max-w-[75%] rounded-t-xl p-3 shadow-lg ${
        isQuestion
          ? "rounded-r-xl rounded-bl-3xl bg-skin-muted"
          : "ml-auto rounded-l-xl rounded-br-3xl bg-skin-secondary"
      }`}
    >
      {message}
      <div
        className={`absolute -bottom-0 h-6 w-6  drop-shadow-sm ${
          isQuestion
            ? "-left-1 -scale-x-1 text-skin-base"
            : "-right-1 text-skin-secondary"
        }`}
      >
        <SpeechBubbleCorner />
      </div>
    </div>
  );
}
