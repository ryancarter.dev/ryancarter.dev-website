import { ComponentStory, ComponentMeta } from "@storybook/react";
import Message from "~/types/message";
import TextMessage from "./textMessage";

export default {
  title: "Components/TextMessage",
  component: TextMessage,
} as ComponentMeta<typeof TextMessage>;

const Template: ComponentStory<typeof TextMessage> = (args: Message) => (
  <TextMessage {...args} />
);

export const QuestionMessage = Template.bind({});
QuestionMessage.args = {
  message: <p>This is a question?</p>,
  type: "question",
};

export const LongQuestionMessage = Template.bind({});
LongQuestionMessage.args = {
  ...QuestionMessage.args,
  message: (
    <p>
      This is a long question that will wrap to multiple lines. This is a long
      question that will wrap to multiple lines.
    </p>
  ),
};

export const AnswerMessage = Template.bind({});
AnswerMessage.args = {
  message: <p>This is an answer</p>,
  type: "answer",
};

export const LongAnswerMessage = Template.bind({});
LongAnswerMessage.args = {
  ...AnswerMessage.args,
  message: (
    <p>
      This is a long answer that will wrap to multiple lines. This is a long
      answer that will wrap to multiple lines.
    </p>
  ),
};
