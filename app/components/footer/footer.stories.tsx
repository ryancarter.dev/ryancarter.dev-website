import { ComponentStory, ComponentMeta } from "@storybook/react";

import Footer, { FooterProps } from "./footer";

export default {
  title: "Components/Footer",
  component: Footer,
} as ComponentMeta<typeof Footer>;

const Template: ComponentStory<typeof Footer> = (args: FooterProps) => (
  <Footer {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  children: (
    <>
      <p>Footer text</p>
      <p>Footer text</p>
      <p>Footer text</p>
    </>
  ),
};
