import { ReactNode } from "react";

export interface FooterProps {
  children?: ReactNode;
}

export default function Footer({ children }: FooterProps) {
  return (
    // todo: add space between and desktop styles
    <footer className="mt-16 flex w-full flex-col items-center justify-center bg-skin-container p-16 text-skin-base">
      {children}
    </footer>
  );
}
