import { ComponentMeta, ComponentStory } from "@storybook/react";
import ThemeSwitcher from "./themeSwitcher";

export default {
  title: "Components/ThemeSwitcher",
  component: ThemeSwitcher,
} as ComponentMeta<typeof ThemeSwitcher>;

const Template: ComponentStory<typeof ThemeSwitcher> = () => <ThemeSwitcher />;

export const Primary = Template.bind({});
