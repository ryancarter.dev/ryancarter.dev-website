import { useContext } from "react";
import { ThemeContext } from "~/contexts/themeContext";
import ThemeCircle from "../themeCircle/themeCircle";

export default function ThemeSwitcher() {
  const { name, setTheme } = useContext(ThemeContext);
  return (
    <div className="flex gap-x-2">
      <div onClick={() => setTheme("green")}>
        <ThemeCircle theme="green" selected={name === "green"} />
      </div>
      <div onClick={() => setTheme("blue")}>
        <ThemeCircle theme="blue" selected={name === "blue"} />
      </div>
    </div>
  );
}
