import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Button, { ButtonProps } from "./button";

export default {
  title: "Components/Button",
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args: ButtonProps) => (
  <Button {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  children: "Button Text",
  className: "",
};

export const Link = Template.bind({});
Link.args = {
  ...Primary.args,
  children: "I'm a link",
  link: "https://www.google.com",
};

export const IconOnly = Template.bind({});
IconOnly.args = {
  icon: faLinkedin,
};

export const TextWithIconToRight = Template.bind({});
TextWithIconToRight.args = {
  ...Primary.args,
  ...IconOnly.args,
};
