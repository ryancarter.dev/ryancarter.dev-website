import { ReactNode } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/free-brands-svg-icons";

export interface ButtonProps {
  children?: ReactNode;
  icon?: IconDefinition;
  className?: string;
  iconPosition?: "left" | "right";
  link?: string;
  onClick?: () => void;
}

export default function Button({
  children,
  icon,
  className,
  iconPosition = "right",
  link,
  onClick,
  ...props
}: ButtonProps) {
  const faIcon = icon ? (
    <FontAwesomeIcon
      className={`${children ? "pl-2" : "text-xl"}`}
      icon={icon}
    />
  ) : null;

  return link ? (
    <a
      className={`inline-block bg-skin-inverted text-center text-skin-base shadow-md hover:cursor-pointer ${
        children
          ? "rounded-xl px-5 py-3"
          : "flex h-12 w-12 items-center justify-center rounded-full"
      } ${className}`}
      href={link}
      target="_blank"
      onClick={onClick}
      {...props}
    >
      {faIcon && iconPosition === "left" && faIcon}
      {children}
      {faIcon && iconPosition === "right" && faIcon}
    </a>
  ) : (
    <button
      className={`inline-block bg-skin-inverted text-center text-skin-base shadow-md hover:cursor-pointer ${
        children
          ? "rounded-xl px-5 py-3"
          : "flex h-12 w-12 items-center justify-center rounded-full"
      } ${className}`}
      onClick={onClick}
      {...props}
    >
      {faIcon && iconPosition === "left" && faIcon}
      {children}
      {faIcon && iconPosition === "right" && faIcon}
    </button>
  );
}
