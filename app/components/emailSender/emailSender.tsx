import { useState } from "react";
import Button from "../button/button";
import Input from "../input/input";

export interface EmailSenderProps {
  className?: string;
}

export default function EmailSender({ className, ...rest }: EmailSenderProps) {
  const [text, updateText] = useState("");

  return (
    <div
      className={`grid gap-4 md:grid-flow-col md:grid-cols-[1fr] ${className}`}
      {...rest}
    >
      <Input
        text={text}
        onChange={updateText}
        className="w-full md:mr-4 md:mb-0"
      />
      <Button
        link={`mailto:hi@ryancarter.dev?subject=I%20have%20more%20questions!&body=${text}`}
      >
        Send
      </Button>
    </div>
  );
}
