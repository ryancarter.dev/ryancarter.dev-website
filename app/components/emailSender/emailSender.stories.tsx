import { ComponentStory, ComponentMeta } from "@storybook/react";

import EmailSender from "./emailSender";

export default {
  title: "Components/EmailSender",
  component: EmailSender,
} as ComponentMeta<typeof EmailSender>;

const Template: ComponentStory<typeof EmailSender> = () => <EmailSender />;

export const Primary = Template.bind({});
