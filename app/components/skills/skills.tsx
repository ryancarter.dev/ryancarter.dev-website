export default function Skills() {
  return (
    <ul>
      <li>
        Core languages: <strong>JavaScript/TypeScript, C#, HTML, CSS</strong>
      </li>
      <li>
        DB: <strong>MySQL, Mongo, GraphQL, DynamoDB</strong>
      </li>
      <li>
        CMS: <strong>Strapi</strong>
      </li>
      <li>
        Front end:{" "}
        <strong>
          React, Remix, Gatsby, Styled Components, Tailwind, Storybook
        </strong>
      </li>
      <li>
        Back end: <strong>Node, C#, ASP.NET Core, OpenAPI</strong>
      </li>
      <li>
        Platforms: <strong>AWS, Netlify, Cloudflare</strong>
      </li>
      <li>
        Dev Ops:{" "}
        <strong>Docker, Kubernetes, GitLab CI, Pulumi, Terraform</strong>
      </li>
    </ul>
  );
}
