export interface InputProps {
  text: string;
  onChange: (text: string) => void;
  className?: string;
}

export default function Input({ text, onChange, className = "" }: InputProps) {
  return (
    <input
      type="text"
      placeholder="I want to know..."
      className={`rounded-xl bg-skin-muted p-3 text-skin-base shadow-md focus:outline-none focus:ring focus:ring-skin-base ${className}`}
      onChange={(e) => onChange(e.target.value)}
      value={text}
    />
  );
}
