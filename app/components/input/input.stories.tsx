import { ComponentStory, ComponentMeta } from "@storybook/react";

import Input, { InputProps } from "./input";

export default {
  title: "Components/Input",
  component: Input,
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args: InputProps) => {
  return <Input {...args} />;
};

export const Primary = Template.bind({});
Primary.args = {
  text: "Input text",
};
