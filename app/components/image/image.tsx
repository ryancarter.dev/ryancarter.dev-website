type Img = {
  src: string;
  type: "image/webp" | "image/jpeg" | "image/png";
  alt: string;
  width: number;
  height: number;
};
export interface ImageProps {
  image: Img;
  fallbackImage: Img;
  rounded?: boolean;
  className?: string;
}

export default function Image({
  image,
  fallbackImage,
  className,
  rounded = true,
}: ImageProps) {
  const styles = `${
    rounded ? "rounded-full" : null
  } w-52 bg-skin-secondary shadow-lg ${className}`;

  return (
    <picture>
      <source
        srcSet={image.src}
        type={image.type}
        width={image.width}
        height={image.height}
      />
      <source
        srcSet={fallbackImage.src}
        type={fallbackImage.type}
        width={fallbackImage.width}
        height={fallbackImage.height}
      />
      <img
        className={styles}
        src={fallbackImage.src}
        alt={fallbackImage.alt}
        width={fallbackImage.width}
        height={fallbackImage.height}
      />
    </picture>
  );
}
