import { ComponentStory, ComponentMeta } from "@storybook/react";

import Image, { ImageProps } from "./image";

export default {
  title: "Components/Image",
  component: Image,
} as ComponentMeta<typeof Image>;

const Template: ComponentStory<typeof Image> = (args: ImageProps) => (
  <Image {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  image: {
    src: "https://picsum.photos/416",
    type: "image/png",
    alt: "Picture of something!",
    width: 416,
    height: 416,
  },
  fallbackImage: {
    src: "https://picsum.photos/416",
    type: "image/png",
    alt: "Picture of something!",
    width: 416,
    height: 416,
  },
};
