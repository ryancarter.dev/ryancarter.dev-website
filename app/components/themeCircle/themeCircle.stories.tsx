import { ComponentMeta, ComponentStory } from "@storybook/react";
import ThemeCircle, { ThemeCircleProps } from "./themeCircle";

export default {
  title: "Components/ThemeCircle",
  component: ThemeCircle,
} as ComponentMeta<typeof ThemeCircle>;

const Template: ComponentStory<typeof ThemeCircle> = (
  args: ThemeCircleProps
) => <ThemeCircle {...args} />;

export const Primary = Template.bind({});
