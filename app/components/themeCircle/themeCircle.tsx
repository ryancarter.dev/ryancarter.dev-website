import { useContext } from "react";
import { ThemeContext } from "~/contexts/themeContext";
import { Theme } from "~/types/theme";
import { themeClasses } from "../theme/theme";

export interface ThemeCircleProps {
  theme: Theme;
  selected?: boolean;
}

function ThemeCircle({ theme, selected }: ThemeCircleProps): JSX.Element {
  const { setTheme } = useContext(ThemeContext);
  return (
    <button
      type="submit"
      onClick={() => setTheme(theme)}
      className={`h-12 w-12 cursor-pointer ${
        selected ? "rounded-full ring ring-black" : ""
      } ${themeClasses[theme]}`}
    >
      <div
        className="inline-block h-12 w-6 origin-center 
            rounded-bl-full rounded-tl-full bg-skin-base"
      ></div>
      <div
        className="inline-block h-12 w-6 origin-center
            -rotate-180 rounded-bl-full rounded-tl-full bg-skin-container"
      ></div>
    </button>
  );
}

export default ThemeCircle;
