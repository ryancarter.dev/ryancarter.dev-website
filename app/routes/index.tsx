import Button from "~/components/button/button";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";

export default function Index() {
  return (
    <div className="flex h-screen items-center justify-center">
      <Button
        link="https://www.linkedin.com/in/ryan-carter-6b3ab567/"
        icon={faLinkedin}
        aria-label="LinkedIn"
      />
    </div>
  );
}
