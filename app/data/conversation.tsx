import Skills from "~/components/skills/skills";
import Message from "~/types/message";
import availability from "./availability";

let messageId = 0;
const getMessageId = () => {
  return (messageId++).toString();
};

const conversation: Message[] = [
  {
    id: getMessageId(),
    message: <p>Who are you?</p>,
    type: "question",
  },
  {
    id: getMessageId(),
    message: (
      <p>
        Hi 👋🏻 I’m Ryan, a Lead Full-Stack Developer based in Manchester, UK.
      </p>
    ),
    type: "answer",
  },
  {
    id: getMessageId(),
    message: <p>Cool, what are your technical skills?</p>,
    type: "question",
  },
  {
    id: getMessageId(),
    message: (
      <>
        <p>Take a look!</p>
        <br />
        <Skills />
      </>
    ),
    type: "answer",
  },
  {
    id: getMessageId(),
    message: (
      <>
        <p>
          However, I can get up and running quickly with any language/framework
          and have also used <strong>Python</strong>, <strong>Vue</strong> and{" "}
          <strong>F#</strong> on past projects.
        </p>
      </>
    ),
    type: "answer",
  },
  {
    id: getMessageId(),
    message: (
      <p>Nice! What about your soft skills? Are you able to talk to people!?</p>
    ),
    type: "question",
  },
  {
    id: getMessageId(),
    message: (
      <p>
        Sure, I’m an effective communicator! I have experience in leading &
        mentoring a team of developers. I am used to interfacing with various
        stakeholders and adding the right level of technicality depending upon
        the audience.
      </p>
    ),
    type: "answer",
  },
  {
    id: getMessageId(),
    message: <p>So, what kind of projects have you done in the past?</p>,
    type: "question",
  },
  {
    id: getMessageId(),
    message: (
      <>
        <p>Now you’re testing me! Let me think 🤔</p>
        <ul className="list-disc pl-4">
          <li>Chatbots</li>
          <li>Custom CRMs</li>
          <li>Custom LMSs</li>
          <li>Tons of API integrations (think Equifax, Land Registry)</li>
          <li>Hybrid mobile apps</li>
          <li>Countless websites</li>
        </ul>
      </>
    ),
    type: "answer",
  },
  {
    id: getMessageId(),
    message: (
      <p>
        Thank you so much for your time. I like it! When are you next available
        to work on my project?
      </p>
    ),
    type: "question",
  },
  {
    id: getMessageId(),
    message: (
      <p>
        No problem! I’m next available on the {availability}. Want to know more?
      </p>
    ),
    type: "answer",
  },
];

export default conversation;
