import { useEffect, useState } from "react";

export const DEFAULT_THEME = "theme-green";

export const withTailwindTheme = (Story, context) => {
  const { theme } = context.globals;
  const [currentTheme, setCurrentTheme] = useState();

  useEffect(() => {
    const bodyTag = document.body;

    bodyTag.classList.add(theme || DEFAULT_THEME);
    if (currentTheme !== theme) {
      bodyTag.classList.remove(currentTheme);
    }
    setCurrentTheme(theme || DEFAULT_THEME);
  }, [theme]);

  return <Story />;
};
