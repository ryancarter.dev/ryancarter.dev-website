const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./app/**/*.{js,ts,jsx,tsx}", "./app/root.tsx"],
  darkMode: "class",
  theme: {
    extend: {
      fontFamily: {
        sans: ["Sora", ...defaultTheme.fontFamily.sans],
      },
      textColor: {
        skin: {
          base: "rgb(var(--color-text-base) / <alpha-value>)",
          inverted: "rgb(var(--color-text-inverted) / <alpha-value>)",
          secondary: "rgb(var(--color-text-secondary) / <alpha-value>)",
        },
      },
      backgroundColor: {
        skin: {
          base: "rgb(var(--color-bg-base) / <alpha-value>)",
          muted: "rgb(var(--color-bg-muted) / <alpha-value>)",
          container: "rgb(var(--color-bg-container) / <alpha-value>)",
          secondary: "rgb(var(--color-bg-secondary) / <alpha-value>)",
          inverted: "rgb(var(--color-bg-inverted) / <alpha-value>)",
        },
      },
      ringColor: {
        skin: {
          base: "rgb(var(--color-bg-base) / <alpha-value>)",
        },
      },
    },
    scale: {
      "-1": "-1",
    },
  },
};
